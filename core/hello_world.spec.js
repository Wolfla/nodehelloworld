const hello_world = require('./hello_world')

test('Correct return string from hello_world', () => {
    expect(hello_world.get_message()).toBe("Hello world!")
});
